package com.zyg.proxy;

public class RentingProxy implements Renting{
    private Landlord landlord = new Landlord();
    public void rent() {
        System.out.println("出租信息");
        System.out.println("寻找客户");
        landlord.rent();
    }
}
