package com.zyg.service;

import com.zyg.dao.UserDao;
import com.zyg.entity.User;
import com.zyg.entity.User2;

public class UserServiceImpl implements UserService{
    private UserDao userDao;

    public UserServiceImpl() {
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void deleteUser(Integer id) {
        System.out.println("删除User");
    }

    @Override
    public void selectUser(Integer id) {
        User2 user2 = userDao.queryUser(id);
        System.out.println(user2);
    }

    @Override
    public void addUser(User user) {
        System.out.println("增加User");
    }

    @Override
    public void updateUser(User user) {
        System.out.println("更新User");
    }
}
