package com.zyg.service;

import com.zyg.dao.UserDao;
import com.zyg.entity.User;

public interface UserService {
    public void deleteUser(Integer id);
    public void selectUser(Integer id);
    public void addUser(User user);
    public void updateUser(User user);
}
