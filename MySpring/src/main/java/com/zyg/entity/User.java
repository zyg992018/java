package com.zyg.entity;
import java.util.*;

public class User {
    private Integer id;
    private String name;
    private String password;
    private Integer age;
    private Date birth;
    private String[] hobbies;
    private Set<String> phones;
    private List<String> names;
    private Map<String,String> countries;
    private Properties files;
    private Address address;

    public User() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", birth=" + birth +
                ", hobbies=" + Arrays.toString(hobbies) +
                ", phones=" + phones +
                ", names=" + names +
                ", countries=" + countries +
                ", files=" + files +
                ", address=" + address +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String[] getHobbies() {
        return hobbies;
    }

    public void setHobbies(String[] hobbies) {
        this.hobbies = hobbies;
    }

    public Set<String> getPhones() {
        return phones;
    }

    public void setPhones(Set<String> phones) {
        this.phones = phones;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public Map<String, String> getCountries() {
        return countries;
    }

    public void setCountries(Map<String, String> countries) {
        this.countries = countries;
    }

    public Properties getFiles() {
        return files;
    }

    public void setFiles(Properties files) {
        this.files = files;
    }

    public User(Integer id, String name, String password, Integer age, Date birth, String[] hobbies, Set<String> phones, List<String> names, Map<String, String> countries, Properties files) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.age = age;
        this.birth = birth;
        this.hobbies = hobbies;
        this.phones = phones;
        this.names = names;
        this.countries = countries;
        this.files = files;
    }
}
