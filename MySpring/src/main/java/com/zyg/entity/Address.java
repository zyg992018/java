package com.zyg.entity;

public class Address {
    private Integer id;
    private String add;

    public Address() {
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", add='" + add + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public Address(Integer id, String add) {
        this.id = id;
        this.add = add;
    }

    public void init_method(){
        System.out.println("初始化");
    }
    public void destroy_method(){
        System.out.println("销毁方法");
    }
}
