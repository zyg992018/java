package com.zyg.advice;

import org.springframework.aop.ThrowsAdvice;

public class MyThrowsAdvice implements ThrowsAdvice {
    public void afterThrowing(Exception e){
        System.out.println("异常通知");
    }
}
