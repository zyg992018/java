package com.zyg.advice;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class MyBeforeActive implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] objects, Object o) throws Throwable {
        //辅助功能
        System.out.println("事务控制");
        System.out.println("日志打印");
    }
}
