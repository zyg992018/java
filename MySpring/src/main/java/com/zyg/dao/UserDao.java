package com.zyg.dao;

import com.zyg.entity.User2;
import org.apache.ibatis.annotations.Param;

public interface UserDao {
    public void deleteUser(Integer id);
    public User2 queryUser(@Param("id") Integer id);
}
