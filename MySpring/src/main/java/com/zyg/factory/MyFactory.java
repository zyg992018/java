package com.zyg.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MyFactory {
    private Properties properties = new Properties();
    public MyFactory(){
        InputStream inputStream = MyFactory.class.getResourceAsStream("/bean.properties");
        try {
            properties.load(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Object getBean(String name){
        String path = properties.getProperty(name);
        Object object = null;
        Class claz = null;
        try {
            claz = Class.forName(path);
            object = claz.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return object;
    }
}
