import com.zyg.dao.UserDao;
import com.zyg.entity.*;
import com.zyg.proxy.Landlord;
import com.zyg.proxy.Renting;
import com.zyg.proxy.RentingProxy;
import com.zyg.service.UserService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SpringTest {
    @Test
    public void test01(){
        //启动工厂
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        //获得实例
        UserDao userDao = (UserDao) context.getBean("UserDao");
        userDao.deleteUser(1);
    }
    @Test
    public void test02(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        User user = (User) context.getBean("User");
        System.out.println((user));
        User user1 = (User) context.getBean("User");
        System.out.println(user==user1);
        System.out.println(user.getAddress()==user1.getAddress());
    }
    @Test
    public void test03(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        Student s  = (Student) context.getBean("Student");
        System.out.println(s);
    }
    @Test
    public void test04() throws SQLException {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        Connection connection = (Connection) context.getBean("connection");
        PreparedStatement preparedStatement = connection.prepareStatement("select * from user");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            String id = resultSet.getString("id");
            System.out.println(id);
        }
        MyConnection myConnection = (MyConnection) context.getBean("&connection");
        System.out.println(myConnection);
    }
    @Test
    public void test05(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        Address address = (Address) context.getBean("address");
        context.close();
    }
    @Test
    public void test06(){
        RentingProxy rentingProxy = new RentingProxy();
        rentingProxy.rent();
    }
    @Test
    public void test07(){
        //被代理目标
        final Landlord landlord = new Landlord();
        //添加额外功能
        InvocationHandler invocationHandler = new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //额外功能
                System.out.println("出租信息");
                System.out.println("寻找客户");
                //核心功能
                landlord.rent();
                return null;
            }
        };
        //动态生成代理类
        /**
         * @Param
         * 类加载器 SpringTest.class.getClassLoader() 当前类的加载器
         * 被代理类的接口类
         * 添加辅助功能的InvocationHandler实例
         * @return
         * 被代理类的接口类的子类
         */
        Renting renting = (Renting) Proxy.newProxyInstance(SpringTest.class.getClassLoader(),landlord.getClass().getInterfaces(),invocationHandler);
        renting.rent();
    }
    @Test
    public void test08(){
        //被代理目标
        final Landlord landlord = new Landlord();
        //
        Enhancer enhancer = new Enhancer();
        //设置父类
        enhancer.setSuperclass(Landlord.class);
        enhancer.setCallback(new org.springframework.cglib.proxy.InvocationHandler() {
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                //添加辅助功能
                System.out.println("出租信息1");
                System.out.println("寻找客户1");
                //核心功能
                landlord.rent();
                return null;
            }
        });
        Landlord proxy = (Landlord)enhancer.create();
        proxy.rent();
    }
    @Test
    public void test09(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");

        UserService proxy = (UserService)context.getBean("UserService");
        System.out.println(proxy.getClass());
        proxy.deleteUser(1);
        System.out.println("======================");
        User user = new User();
        proxy.addUser(user);
        System.out.println("======================");
        proxy.updateUser(user);
        System.out.println("======================");
        proxy.selectUser(1);
    }
    @Test
    public void test10(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        SqlSessionFactory sqlSessionFactory = (SqlSessionFactory) context.getBean("sqlSessionFactory");
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserDao userDao = sqlSession.getMapper(UserDao.class);
        User2 user2 = userDao.queryUser(2);
        System.out.println(user2);
    }
    @Test
    public void test11(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        UserDao userDao = (UserDao) context.getBean(("userDao"));
        User2 user2 = userDao.queryUser(2);
        System.out.println(user2);
    }
    @Test
    public void test12(){
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring-context.xml");
        UserService userService = (UserService) context.getBean("UserService");
        userService.selectUser(2);
    }
}
