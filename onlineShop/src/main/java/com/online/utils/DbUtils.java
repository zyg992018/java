package com.online.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.io.File;
import java.sql.Connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbUtils {
    private static final ThreadLocal<Connection> THREAD_LOCAL = new ThreadLocal<Connection>();
    private static DruidDataSource ds = null;
    static {
        Properties properties = new Properties();
        InputStream inputStream = DbUtils.class.getResourceAsStream("db.properties");
        System.out.println(inputStream);
        try {
            properties.load(inputStream);
            ds = (DruidDataSource) DruidDataSourceFactory.createDataSource(properties);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection(){
        Connection connection = THREAD_LOCAL.get();
        if(connection == null){
            try {
                connection = ds.getConnection();
                connection.setAutoCommit(false);
                THREAD_LOCAL.set(connection);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return connection;
    }
    public static void begin(){
        Connection connection=null;
        connection = THREAD_LOCAL.get();
        if(connection==null){
            connection = getConnection();
        }
    }
    public static void commit(){
        Connection connection = THREAD_LOCAL.get();
        if(connection!=null){
            try {
                connection.commit();
                closeAll(connection,null,null);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    public static void rollback(){
        Connection connection =THREAD_LOCAL.get();
        if(connection!=null){
            try {
                connection.rollback();
                closeAll(connection,null,null);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
    public static void closeAll(Connection connection, Statement statement, ResultSet resultSet){
        if(resultSet!=null){
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
