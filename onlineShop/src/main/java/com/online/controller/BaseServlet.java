package com.online.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method");
        Class<? extends BaseServlet> clazz = this.getClass();
        System.out.println(method+111111);
        try {
            if(method==null){
                method = "index";
            }
            Method method1 = clazz.getMethod(method,HttpServletRequest.class,HttpServletResponse.class);
            String url = (String)method1.invoke(this,req,resp);
            if(url!=null){
                if(url.startsWith("forward:"))
                    req.getRequestDispatcher(url.substring(url.indexOf(":")+1)).forward(req,resp);
                if(url.startsWith("redirect:"))
                    resp.sendRedirect(url.substring(url.indexOf(":")+1));
                else {
                    resp.getWriter().println(url);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            failed(req,resp);
        }
    }
    public void failed(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println("参数错误");
    }
    public void index(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(req.getContextPath()+"/index.jsp");
    }
}
