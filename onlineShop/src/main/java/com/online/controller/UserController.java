package com.online.controller;

import com.online.entity.User;
import com.online.service.UserService;
import com.online.service.impl.UserServiceImpl;
import java.util.Base64;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/user")
public class UserController extends BaseServlet {

    public String login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String code = req.getParameter("code");
        if(code!=null){
            if(code.equalsIgnoreCase((String)req.getSession().getAttribute("code"))){
                String username = req.getParameter("username");
                String password = req.getParameter("password");
                String auto = req.getParameter("auto");
                if(username!=null && password!=null){
                    User user = new UserServiceImpl().getUser(username);
                    if(user.getPassword().equals(password)){
                        if(auto!=null && !auto.trim().equals("")){
                            String content = username+":"+password;
                            content = new String(Base64.getEncoder().encode(content.getBytes()));
                            Cookie  cookie = new Cookie("auto",content);
                            cookie.setPath("/");
                            cookie.setMaxAge(14*24*3600);
                            resp.addCookie(cookie);
                        }else{
                            Cookie  cookie = new Cookie("auto","");
                            cookie.setPath("/");
                            cookie.setMaxAge(0);
                            resp.addCookie(cookie);
                        }
                        HttpSession session = req.getSession();
                        session.setAttribute("user",user);
                        return "redirect:/online/index.jsp";
                    }
                }
            }
        }
        return "redirect:/online/user/login.jsp";
    }

    public String registers(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User user = new User();
        user.setUsername(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        user.setEmail(req.getParameter("email"));
        user.setSex(req.getParameter("sex"));
        user.setStatus(0);
        user.setRole(0);
        user.setCode("" + Math.random());
        System.out.println(user);
        int num = new UserServiceImpl().registerUser(user);
        if (num == 0) {
            req.setAttribute("msg","注册失败");
            return "forward:user/register.jsp";
        } else {
            req.setAttribute("msg","注册成功");
            return "forward:user?method=login";
        }
    }

    public String logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie cookie = new Cookie("auto","");
        cookie.setPath("/");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        return "redirect:/onlineShop/user/login.jsp";
    }

    public String check(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String username = req.getParameter("username");
        if(username==null)return "1";
        UserService userService = new UserServiceImpl();
        if(userService.checkUser(username)){
            return "1";
        }
        return "0";
    }
}
