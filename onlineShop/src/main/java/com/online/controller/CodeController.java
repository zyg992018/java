package com.online.controller;

import cn.dsna.util.images.ValidateCode;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@WebServlet(value = "/code")
public class CodeController extends BaseServlet {
    public void getCode(HttpServletRequest req, HttpServletResponse resp){
        ValidateCode validateCode = new ValidateCode(100,35,4,20);
        String code = validateCode.getCode();
        req.getSession().setAttribute("code",code);
        try {
            validateCode.write(resp.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
