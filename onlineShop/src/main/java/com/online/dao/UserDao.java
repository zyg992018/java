package com.online.dao;

import com.online.entity.User;

/**
 * 用户表操作
 */
public interface UserDao {

    /**
     * 根据username查询用户
     * @param username 用户名
     * @return User对象
     */
    User selectUser(String username);

    /**
     * 插入用户数据
     * @param user 用户实体
     * @return 插入行数
     */
    int insertUser(User user);
}
