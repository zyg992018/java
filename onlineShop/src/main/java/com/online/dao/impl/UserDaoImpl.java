package com.online.dao.impl;

import com.online.dao.UserDao;
import com.online.entity.User;
import com.online.utils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    private QueryRunner queryRunner = new QueryRunner();
    public User selectUser(String username) {
        User user = null;
        try {
             user = queryRunner.query(DbUtils.getConnection(),"SELECT * FROM User WHERE username=?",
                    new BeanHandler<User>(User.class),username);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return user;
    }

    public int insertUser(User user) {
        int num = 0;
        if(user!=null){
            try {
                num = queryRunner.update(DbUtils.getConnection(),"insert into User(username,password,email,sex,status,code,role)  values(?,?,?,?,?,?,?)",
                        user.getUsername(),user.getPassword(),user.getEmail(),user.getSex(),user.getStatus(),user.getCode(),user.getRole());
                System.out.println(num);
                return num;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return num;
            }
        }
        return num;
    }
}
