package com.online.filter;

import com.online.entity.User;
import com.online.service.UserService;
import com.online.service.impl.UserServiceImpl;
import java.util.Base64;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns ="/user/login.jsp")
public class AutoFilter  implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        Cookie[] cookies = req.getCookies();
        if(cookies!=null){
            String content = null;
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("auto")){
                    content = cookie.getValue();
                    break;
                }
            }
            if(content!=null){
                Base64.Decoder decoder = Base64.getDecoder();
                content = new String(decoder.decode(content));
                System.out.println(content);
                String[] split = content.split(":");
                String username = split[0];
                String password = split[1];
                UserService userService = new UserServiceImpl();
                try{
                    User user = userService.getUser(username);
                    if(user!=null && user.getPassword().equals(password)){
                        HttpSession session = req.getSession();
                        session.setAttribute("user",user);
                        resp.sendRedirect("/onlineShop/index.jsp");
                    }
                }catch (Exception e){
                    filterChain.doFilter(req,resp);
                }
            }
        }
        filterChain.doFilter(req,resp);
    }

    public void destroy() {

    }
}
