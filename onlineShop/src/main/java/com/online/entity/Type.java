package com.online.entity;

public class Type {
    private int t_d;
    private String t_name;
    private String t_info;

    public Type() {
    }

    public Type(int t_d, String t_name, String t_info) {
        this.t_d = t_d;
        this.t_name = t_name;
        this.t_info = t_info;
    }

    public int getT_d() {
        return t_d;
    }

    public void setT_d(int t_d) {
        this.t_d = t_d;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public String getT_info() {
        return t_info;
    }

    public void setT_info(String t_info) {
        this.t_info = t_info;
    }

    @Override
    public String toString() {
        return "Type{" +
                "t_d=" + t_d +
                ", t_name='" + t_name + '\'' +
                ", t_info='" + t_info + '\'' +
                '}';
    }
}
