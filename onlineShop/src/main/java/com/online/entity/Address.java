package com.online.entity;

public class Address {
    private int a_id;
    private String a_name;
    private String a_phone;
    private String s_detail;
    private int a_state;
    private int u_id;

    @Override
    public String toString() {
        return "Address{" +
                "a_id=" + a_id +
                ", a_name='" + a_name + '\'' +
                ", a_phone='" + a_phone + '\'' +
                ", s_detail='" + s_detail + '\'' +
                ", a_state=" + a_state +
                ", u_id=" + u_id +
                '}';
    }

    public Address(){}

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public String getA_phone() {
        return a_phone;
    }

    public void setA_phone(String a_phone) {
        this.a_phone = a_phone;
    }

    public String getS_detail() {
        return s_detail;
    }

    public void setS_detail(String s_detail) {
        this.s_detail = s_detail;
    }

    public int getA_state() {
        return a_state;
    }

    public void setA_state(int a_state) {
        this.a_state = a_state;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public Address(int a_id, String a_name, String a_phone, String s_detail, int a_state, int u_id) {
        this.a_id = a_id;
        this.a_name = a_name;
        this.a_phone = a_phone;
        this.s_detail = s_detail;
        this.a_state = a_state;
        this.u_id = u_id;
    }
}
