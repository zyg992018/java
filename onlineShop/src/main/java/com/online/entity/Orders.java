package com.online.entity;

import java.util.Date;

public class Orders {
    private String o_id;
    private double o_count;
    private Date o_time;
    private int o_state;
    private int u_id;
    private int a_id;

    public Orders() {
    }

    public Orders(String o_id, double o_count, Date o_time, int o_state, int u_id, int a_id) {
        this.o_id = o_id;
        this.o_count = o_count;
        this.o_time = o_time;
        this.o_state = o_state;
        this.u_id = u_id;
        this.a_id = a_id;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "o_id='" + o_id + '\'' +
                ", o_count=" + o_count +
                ", o_time=" + o_time +
                ", o_state=" + o_state +
                ", u_id=" + u_id +
                ", a_id=" + a_id +
                '}';
    }

    public String getO_id() {
        return o_id;
    }

    public void setO_id(String o_id) {
        this.o_id = o_id;
    }

    public double getO_count() {
        return o_count;
    }

    public void setO_count(double o_count) {
        this.o_count = o_count;
    }

    public Date getO_time() {
        return o_time;
    }

    public void setO_time(Date o_time) {
        this.o_time = o_time;
    }

    public int getO_state() {
        return o_state;
    }

    public void setO_state(int o_state) {
        this.o_state = o_state;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }
}
