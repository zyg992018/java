package com.online.entity;

public class Item {
    private int i_id;
    private double i_count;
    private int i_num;
    private String o_id;
    private int p_id;
    public Item(){}

    @Override
    public String toString() {
        return "Item{" +
                "i_id=" + i_id +
                ", i_count=" + i_count +
                ", i_num=" + i_num +
                ", o_id='" + o_id + '\'' +
                ", p_id=" + p_id +
                '}';
    }

    public int getI_id() {
        return i_id;
    }

    public void setI_id(int i_id) {
        this.i_id = i_id;
    }

    public double getI_count() {
        return i_count;
    }

    public void setI_count(double i_count) {
        this.i_count = i_count;
    }

    public int getI_num() {
        return i_num;
    }

    public void setI_num(int i_num) {
        this.i_num = i_num;
    }

    public String getO_id() {
        return o_id;
    }

    public void setO_id(String o_id) {
        this.o_id = o_id;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public Item(int i_id, double i_count, int i_num, String o_id, int p_id) {
        this.i_id = i_id;
        this.i_count = i_count;
        this.i_num = i_num;
        this.o_id = o_id;
        this.p_id = p_id;
    }
}
