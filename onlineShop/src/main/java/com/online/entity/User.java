package com.online.entity;

public class User {
    private int u_id;
    private String username ;
    private String password;
    private String email;
    private String sex;
    private int status;
    private String code;
    private int role;

    public User() {
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public User(int u_id, String username, String password, String email, String sex, int status, String code, int role) {
        this.u_id = u_id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.sex = sex;
        this.status = status;
        this.code = code;
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "u_id=" + u_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", sex='" + sex + '\'' +
                ", status=" + status +
                ", code='" + code + '\'' +
                ", role=" + role +
                '}';
    }
}
