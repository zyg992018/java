package com.online.service.impl;

import com.online.dao.UserDao;
import com.online.dao.impl.UserDaoImpl;
import com.online.entity.User;
import com.online.service.UserService;
import com.online.utils.DbUtils;


public class UserServiceImpl implements UserService {
    public boolean checkUser(String username) {
        DbUtils.begin();
        User user = null;
        try{
            user = new UserDaoImpl().selectUser(username);
            DbUtils.commit();
        }catch (Exception e){
            DbUtils.rollback();
        }
        if(user==null)return false;
        else return true;
    }

    public User getUser(String username){
        DbUtils.begin();
        User user = null;
        try{
            user = new UserDaoImpl().selectUser(username);
            DbUtils.commit();
        }catch (Exception e){
            DbUtils.rollback();
        }
        return user;
    }

    public int registerUser(User user) {
        DbUtils.begin();
        try {
            UserDao userDao = new UserDaoImpl();
            int num = userDao.insertUser(user);
            DbUtils.commit();
            return num;
        }catch (Exception e){
            DbUtils.rollback();
            e.printStackTrace();
        }
        return 0;
    }
}
