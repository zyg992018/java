package com.online.service;

import com.online.entity.User;

public interface UserService {
    /**
     * 检测用户名是否存在
     * @param username 用户名
     * @return true 存在 false 不存在
     */
    boolean checkUser(String username);

    /**
     * 用户注册
     * @param user 用户实体
     * @return 插入行数
     */
    int registerUser(User user);

    /**
     * 获得用户信息
     * @param username 用户名
     * @return 用户实体
     */
    User getUser(String username);
}
