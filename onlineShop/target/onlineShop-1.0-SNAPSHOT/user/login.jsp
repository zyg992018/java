<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        body{
            background-color: #f9f9f9;;
        }
        .login{
            width: 782px;
            height: 520px;
            background-color: #fff;
            margin: 100px auto;
        }
        .login>span{
            width: 49px;
            height: 48px;
            margin: 0 auto;
            display: block;
            cursor: default;
            background-image: url(../img/logo/milogo.png);
        }
        .login>h1{
            margin: 20px 0;
            text-align: center;
            font-weight: normal;
            font-size: 30px;
        }
        .login>form{
            text-align: center;
            box-sizing: border-box;
            width: 100%;
            height: 300px;
            position: relative;
        }
        .login>form>label{
            margin-top:15px ;
            box-sizing: border-box;
            padding-right: 30px;
            text-align: right;
            display: inline-block;
            width: 100px;
            height: 30px;
            line-height: 30px;
        }

        .login>form>input:nth-of-type(1),.login>form>input:nth-of-type(2),.login>form>input:nth-of-type(3){
            margin-top: 20px;
            width: 250px;
            height: 30px;
            border-radius: 2px;
            outline: none;
            box-sizing: border-box;
            border:1px solid #ccc;
            padding-left: 10px;
            background-color: transparent;
        }
        .login>form>div{
            width: 300px;
            height: 50px;
            margin: 40px auto;
            box-sizing: border-box;
            padding:0 40px;
            text-align: center;
        }
        .login>form>div>input{
            cursor: pointer;
            display: inline-block;
            width: 50px;
            height: 30px;
            border-radius: 2px;
            background-color: transparent;
            border: 1px solid #ccc;
            outline: none;
            margin-left: 30px;
        }
        .login>form>div>input:hover{
            background-color: orangered;
            color: white;
        }
        #code{
            cursor: pointer;
            position: absolute;
            top: 118px;
            right: 100px;
        }
        #msg1{
            position: absolute;
            top: 25px;
            left: 585px;
            color: red;
        }
    </style>
</head>
<body>
    <div class="login">
        <span></span>
        <h1>登录</h1>
        <form action="/onlineShop/user?method=login" method="post">
            <label for="user-name-label">用户名</label><input type="text" name="username" id="user-name-label"><span id="msg1"></span><br>
            <label for="user-password-label">密码</label><input type="password" name="password" id="user-password-label"><br>
            <label for="user-code-label">验证码</label><input type="text" name="code" id="user-code-label"><img src="/onlineShop/code?method=getCode&t=0" alt="code" id="code"><br>
            <input type="radio" name="auto"><span>自动登录</span><br>
            <div>
                <input type="reset" value="清除">
                <input type="submit" value="登录">
            </div>
        </form>
    </div>
    <script src="../js/jq.min.js"></script>
    <script>
        $("#code").click(function(){
            $(this).attr("src","/onlineShop/code?method=getCode&t="+Math.random())
            console.log(this)
        })
        $("input[name='username']").change(function(){
            console.log(111)
            $.get("http://localhost:8080/onlineShop/user","method=check"+"&username="+this.value,function (data) {
                if(data.trim()=="0"){
                    $("#msg1").text("用户名不存在")
                }else{
                    $("#msg1").text("")
                }
            })
        })
    </script>
</body>
</html>
