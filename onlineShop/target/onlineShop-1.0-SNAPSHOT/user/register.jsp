<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core%>
<html>
<head>
    <title>注册</title>
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        body{
            background-color: #f9f9f9;;
        }
        .register{
            width: 782px;
            height: 520px;
            background-color: #fff;
            margin: 100px auto;
        }
        .register>span{
            width: 49px;
            height: 48px;
            margin: 0 auto;
            display: block;
            cursor: default;
            background-image: url(../img/logo/milogo.png);
        }
        .register>h1{
            margin: 20px 0;
            text-align: center;
            font-weight: normal;
            font-size: 30px;
        }
        .register>form{
            text-align: center;
            box-sizing: border-box;
            width: 100%;
            height: 300px;
            position: relative;
        }
        .register>form>label{
            margin-top:15px ;
            box-sizing: border-box;
            padding-right: 30px;
            text-align: right;
            display: inline-block;
            width: 100px;
            height: 30px;
            line-height: 30px;
        }
        .register>form>input:last-of-type{
            margin-left: 15px;

        }
        .register>form>input:nth-of-type(1),.register>form>input:nth-of-type(2),.register>form>input:nth-of-type(3),.register>form>input:nth-of-type(4){
            margin-top: 20px;
            width: 250px;
            height: 30px;
            border-radius: 2px;
            outline: none;
            box-sizing: border-box;
            border:1px solid #ccc;
            padding-left: 10px;
            background-color: transparent;
        }
        .register>form>div{
            width: 300px;
            height: 50px;
            margin: 40px auto;
            box-sizing: border-box;
            padding:0 40px;
            text-align: center;
        }
        .register>form>div>input{
            cursor: pointer;
            display: inline-block;
            width: 50px;
            height: 30px;
            border-radius: 2px;
            background-color: transparent;
            border: 1px solid #ccc;
            outline: none;
            margin-left: 30px;
        }
        .register>form>div>input:hover{
            background-color: orangered;
            color: white;
        }
        #msg1{
            position: absolute;
            top: 25px;
            left: 585px;
            color: red;
        }
    </style>
</head>
<body>
    <div class="register">
        <span></span>
        <h1>注册小米账号</h1>
        <form action="/onlineShop/user?method=registers" method="post">
            <label for="user-name-label">用户名</label><input type="text" placeholder="Username" id="user-name-label" name="username"><span id="msg1"></span></span><br>
            <label for="user-password-label">密码</label><input type="password" name="password" id="user-password-label" placeholder="Password"><br>
            <label for="user-password_again-label">确认密码</label><input type="password" id="user-password_again-label" placeholder="PasswordAgain"><br>
            <label for="user-email-label">邮箱</label><input type="email" id="user-email-label" name="email" placeholder="Email"><br>
            <label>性别</label><input type="radio" name="sex" value="女" checked> 女<input type="radio" name="sex" value="男"> 男<br>
            <div>
                <input type="reset" value="清除">
                <input type="submit" value="注册">
            </div>
        </form>
    </div>
    <script src="../js/jq.min.js"></script>
    <script>
        $("input[name='username']").change(function(){
            console.log(111)
            $.get("http://localhost:8080/onlineShop/user","method=check"+"&username="+this.value,function (data) {
                if(data.trim()=="0"){
                    $("#msg1").text("")
                }else{
                    $("#msg1").text("用户名已存在")
                }
            })
        })
    </script>
    <c: if test="${"msg"}!=null">
        <div id="msg">注册失败</div>
    </c:>
    <%
        request.setAttribute("msg",null);
    %>
</body>
</html>