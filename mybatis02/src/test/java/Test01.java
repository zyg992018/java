import com.zyg.dao.*;
import com.zyg.entity.*;
import com.zyg.utils.DbUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test01 {
    public static void main(String[] args) throws IOException {
        UserDao userMapper = DbUtils.getMapper(UserDao.class);
        User user = userMapper.selectUserById(null,"张永刚");
        System.out.println(user);
        PassengersDao passengersDao = DbUtils.getMapper(PassengersDao.class);
        Passengers passengers = passengersDao.queryPassportById(1);
        System.out.println(passengers);
        PassportsDao passportsDao = DbUtils.getMapper(PassportsDao.class);
        Passports passports = passportsDao.queryPassportById(1);
        System.out.println(passports);
        SDao sDao = DbUtils.getMapper(SDao.class);
        S s = sDao.querySBySno(12345);
        System.out.println(s);
    }
    public void test01(){
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserDao userMapper = sqlSession.getMapper(UserDao.class);
        User user =  userMapper.selectUserById(10,null);
        System.out.println(user);
//        User user1 = new User(null,"玥玥2","123456","女",18);
//        userMapper.insertUser(user1);
//        System.out.println(user1);
        StudentDao studentMapper = sqlSession.getMapper(StudentDao.class);
        Student student = new Student(null,"玥玥",0,18,"aa","bb","cc");
        studentMapper.insertStudent(student);
        System.out.println(student);
        sqlSession.commit();
    }
}
