package com.zyg.dao;

import com.zyg.entity.S;
import org.apache.ibatis.annotations.Param;

public interface SDao {
    S querySBySno(@Param("sno") int sno);
}
