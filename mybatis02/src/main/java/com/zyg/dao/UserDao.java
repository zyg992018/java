package com.zyg.dao;

import com.zyg.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserDao {
    public User selectUserById(@Param("id") Integer id,@Param("username") String username);
    public Integer insertUser(User user);
}
