package com.zyg.dao;

import com.zyg.entity.Student;

public interface StudentDao {
    public Integer insertStudent(Student student);
}
