package com.zyg.dao;

import com.zyg.entity.Passengers;
import org.apache.ibatis.annotations.Param;

public interface PassengersDao {
    Passengers queryPassportById(@Param("id") Integer id);
}
