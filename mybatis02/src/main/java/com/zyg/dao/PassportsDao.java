package com.zyg.dao;

import com.zyg.entity.Passports;
import org.apache.ibatis.annotations.Param;

public interface PassportsDao {
    Passports queryPassportById(@Param("id") Integer id);
}
