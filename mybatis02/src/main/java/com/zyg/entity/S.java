package com.zyg.entity;

import java.util.LinkedList;
import java.util.List;

public class S {
    private Integer sno;
    private String sname;
    private Integer age;
    private Boolean sex;
    private List<SC> sc = new LinkedList<SC>();
    public S(){}

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("S{" +
                "sno=" + sno +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", sc={");
        int count = 0;
        while (count < sc.size()){
            s.append(sc.get(count).toString1()).append(" ");
            count++;
        }
        s.append("}}");
        return s.toString();
    }

    public String toString1() {
        return "S{" +
                "sno=" + sno +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    public Integer getSno() {
        return sno;
    }

    public void setSno(Integer sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public S(Integer sno, String sname, Integer age, Boolean sex) {
        this.sno = sno;
        this.sname = sname;
        this.age = age;
        this.sex = sex;
    }
}
