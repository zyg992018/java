package com.zyg.entity;

public class C {
    private Integer cno;
    private String cname;

    public C() {
    }

    @Override
    public String toString() {
        return "C{" +
                "cno=" + cno +
                ", cname='" + cname + '\'' +
                '}';
    }

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public C(Integer cno, String cname) {
        this.cno = cno;
        this.cname = cname;
    }
}
