package com.zyg.entity;

public class Student {
    private String sno;
    private String sname;
    private Integer sex;
    private Integer age;
    private String xueyuan;
    private String major;
    private String school;

    @Override
    public String toString() {
        return "Student{" +
                "sno='" + sno + '\'' +
                ", sname='" + sname + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                ", xueyuan='" + xueyuan + '\'' +
                ", major='" + major + '\'' +
                ", school='" + school + '\'' +
                '}';
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getXueyuan() {
        return xueyuan;
    }

    public void setXueyuan(String xueyuan) {
        this.xueyuan = xueyuan;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Student(String sno, String sname, Integer sex, Integer age, String xueyuan, String major, String school) {
        this.sno = sno;
        this.sname = sname;
        this.sex = sex;
        this.age = age;
        this.xueyuan = xueyuan;
        this.major = major;
        this.school = school;
    }
}
