package com.zyg.entity;

public class SC {
    private Integer id;
    private Integer Score;
    private S s;
    private C c;
    public SC() {
    }

    public String toString1() {
        return "SC{" +
                "id=" + id +
                ", Score=" + Score +
                ", C=" + c.toString() +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return Score;
    }

    public void setScore(Integer score) {
        Score = score;
    }

    public SC(Integer id, Integer score) {
        this.id = id;
        Score = score;
    }
}
