package com.zyg.entity;

import java.sql.Date;

public class Passports {
    private Integer id;
    private String nationality;
    private Date expire;
    private Passengers passengers;
    public Passports(){}
    public String toString1() {
        return "Passports{" +
                "id=" + id +
                ", nationality='" + nationality + '\'' +
                ", expire=" + expire +
                '}';
    }

    @Override
    public String toString() {
        return "Passports{" +
                "id=" + id +
                ", nationality='" + nationality + '\'' +
                ", expire=" + expire +
                ", passengers=" + passengers.toString1() +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public Passengers getPassengers() {
        return passengers;
    }

    public void setPassengers(Passengers passengers) {
        this.passengers = passengers;
    }

    public Passports(Integer id, String nationality, Date expire, Passengers passengers) {
        this.id = id;
        this.nationality = nationality;
        this.expire = expire;
        this.passengers = passengers;
    }
}
