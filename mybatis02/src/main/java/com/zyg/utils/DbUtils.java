package com.zyg.utils;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * 1.加载配置
 * 2.创建SqlSessionFactory
 * 3.创建SqlSession
 * 4.事务处理
 * 5.mapper获取
 */
public class DbUtils {
    private static SqlSessionFactory sqlSessionFactory;
    private final static ThreadLocal<SqlSession> THREAD_LOCAL = new ThreadLocal<SqlSession>();
    static {
        try {
            InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static SqlSession getSqlSession(){
        SqlSession sqlSession = THREAD_LOCAL.get();
        if(sqlSession == null){
            sqlSession =  sqlSessionFactory.openSession();
            THREAD_LOCAL.set(sqlSession);
        }
        return sqlSession;
    }
    public static void commit(){
        SqlSession sqlSession = getSqlSession();
        sqlSession.commit();
        close();
    }
    public static void rollback(){
        SqlSession sqlSession = getSqlSession();
        sqlSession.rollback();
        close();
    }
    public static void close(){
        SqlSession sqlSession = getSqlSession();
        sqlSession.close();
    }
    public static <T> T getMapper(Class<T> mapper){
        SqlSession sqlSession = getSqlSession();
        return sqlSession.getMapper(mapper);
    }
}
