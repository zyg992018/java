package com.zyg.utils;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.pooled.PooledDataSourceFactory;

public class DataSource extends PooledDataSourceFactory {
    public DataSource(){
            this.dataSource = new DruidDataSource();
    }
}
