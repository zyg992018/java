package com.zyg.myspringboot.confing;

import com.zyg.myspringboot.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfig {
    @Bean(name = "user1")//没有name默认方法名
    public User user(){
        User user = new User();
        user.setId(1);
        user.setName("张三");
        return user;
    }
    /**
     * <bean id ="user" class="com.zyg.entity.User"></bean>
     */
}
