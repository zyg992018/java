package com.zyg.myspringboot;

import com.zyg.myspringboot.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {
//    @Autowired
    @Resource(name = "user1")
    private User user;

    @GetMapping("/test")
    public String test01(){
        return "hello spring-boot";
    }

    @GetMapping("test2")
    public User test02(){
        return user;
    }
}
