package com.zyg.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyInterceptor  implements HandlerInterceptor {
    //在handler之前执行
    //再次定义handler重复功能
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("定义重复功能");
//        response.sendRedirect("");
        //中断请求
        //return false;
        //放行
        return true;
    }

    //在handler之后执行
    //改变handler执行后的数据
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("处理执行后的数据");
    }

    //在视图渲染完毕之后执行
    //资源回收
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("视图渲染完毕，资源回收");
    }
}
