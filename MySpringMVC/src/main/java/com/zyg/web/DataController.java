package com.zyg.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/data")
@SessionAttributes(names={"city","street"})
public class DataController {
    @RequestMapping("/test")
    public String test1(HttpServletRequest request, HttpSession session, Model model){
        request.setAttribute("id",12334);
        session.setAttribute("age",18);
        model.addAttribute("name","张三");
        return "data";
    }
    @RequestMapping("test2")
    public String test2(Model model){
        model.addAttribute("city","北京");
        model.addAttribute("street","长安街");
        return "data2";
    }
    @RequestMapping("/test3")
    public String test3(SessionStatus sessionStatus){
        sessionStatus.setComplete();
        return "data2";
    }
    @RequestMapping("/test4")
    public ModelAndView test4(){
        ModelAndView modelAndView = new ModelAndView("forward:/data2.jsp");
        modelAndView.addObject("city","北京");
        modelAndView.addObject("street","长安街");
        return modelAndView;
    }
}
