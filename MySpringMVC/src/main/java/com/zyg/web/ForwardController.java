package com.zyg.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/forward")
public class ForwardController {
    @RequestMapping("/test1")
    public String test1(){
        return "forward:/hello.jsp";
    }
    @RequestMapping("test2")
    public String test2(){
        return null;
    }
}
