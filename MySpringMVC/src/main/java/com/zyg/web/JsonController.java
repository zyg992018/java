package com.zyg.web;

import com.zyg.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@Controller
@RequestMapping("/json")
@RestController
public class JsonController {
    @RequestMapping("/test1")
//    @ResponseBody
    public User test1(){
        User user = new User(12,"小红","1234567","女",18);
        return user;
    }
//    @ResponseBody
    @RequestMapping(value = "/test2",produces = "text/html;charset=utf-8")
    public String test2(){
        return "你好";
    }
    @RequestMapping("test3")
    public String test3(@RequestBody User user){
        System.out.println(user);
        return "ok";
    }
}
