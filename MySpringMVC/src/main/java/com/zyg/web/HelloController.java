package com.zyg.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller//后端控制器
@RequestMapping("/hello")//访问路径
public class HelloController {
    @RequestMapping("/test1")
    public String hello(){
        System.out.println("hello");
        return "hello";
    }
    @RequestMapping("/*")
    public String last(){
        System.out.println("hello");
//        return "forward:/hello.jsp";//forward后面是完整路径
//        return "forward:/param/test3/1234";
        return "forward:test1";
    }
    @RequestMapping("/error")
    public String error(){
        Integer a= 1/0;
        return "forward:hello";
    }
    @RequestMapping("test2")
    public String test2(){
        System.out.println("视图渲染");
        return "forward:error";
    }
}
