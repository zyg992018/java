package com.zyg.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/upload")
public class UploadFileController {
    @RequestMapping("/test1")
    public String test1(MultipartFile file, HttpSession session){
        System.out.println(file.getName());
        System.out.println(file.getOriginalFilename());
        System.out.println(file.getSize());
        System.out.println(file.getContentType().split("/")[1]);
        try {
            System.out.println(session.getServletContext().getRealPath("/upload")+"/"+file.getOriginalFilename());
            file.transferTo(new File(session.getServletContext().getRealPath("/upload")+"/"+file.getOriginalFilename()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "hello";
    }
}
