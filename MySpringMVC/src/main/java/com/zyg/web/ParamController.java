package com.zyg.web;

import com.zyg.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/param")
public class ParamController {
    @RequestMapping("/test1")
    //同名接受参数 http://localhost:8080/param/test1?id=1&name=%E5%BC%A0%E4%B8%89&gender=true
    public String test1(Integer id,String name, Boolean gender){
        System.out.println(id);
        System.out.println(name);
        System.out.println(gender);
        return "hello";
    }
    //id=10&username=root&password=120&sex=男&age=18
    @RequestMapping("/test2")
    public String test2(User user){
        System.out.println(user);
        return "hello";
    }
    //路径传参
    @RequestMapping("/test3/{id}")
    public String test3(@PathVariable("id") Integer id){
        System.out.println(id);
        return  "hello";
    }
}
