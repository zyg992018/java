package com.zyg.entity;

import java.sql.Date;

public class User {
    private Integer id;
    private String username;
    private String sex;
    private Date register_time;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", register_time='" + register_time + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getRegister_time() {
        return register_time;
    }

    public void setRegister_time(Date register_time) {
        this.register_time = register_time;
    }

    public User(Integer id, String username, String sex, Date register_time) {
        this.id = id;
        this.username = username;
        this.sex = sex;
        this.register_time = register_time;
    }
}
