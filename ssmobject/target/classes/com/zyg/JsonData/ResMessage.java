package com.zyg.JsonData;

public class ResMessage {
    private String message;
    private Boolean status;

    public ResMessage() {
    }

    @Override
    public String toString() {
        return "ResMessage{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ResMessage(String message, Boolean status) {
        this.message = message;
        this.status = status;
    }
}
