package com.zyg.mapper;

import com.zyg.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface UserMapper {
//    @Select("SELECT * FROM USER")
    List<User> getAllUsers();

    @Delete("DELETE FROM user WHERE id = #{id}")
    int deleteUser(@Param("id") int id);
}
