package com.zyg.controller;

import com.github.pagehelper.PageInfo;
import com.zyg.JsonData.ResMessage;
import com.zyg.JsonData.TableData;
import com.zyg.entity.User;
import com.zyg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("users")
public class UserController {
    @Autowired(required = true)
    private UserService userService;

    @RequestMapping("get")
    @ResponseBody
    public TableData getUserList(int page, int limit){
        PageInfo<User> pageInfo = userService.getUserList(page,limit);
        TableData tableData = new TableData();
        tableData.setCode(0);
        tableData.setMsg("成功");
        tableData.setCount(pageInfo.getTotal());
        tableData.setData(pageInfo.getList());
        return tableData;
    }
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @ResponseBody
    public ResMessage deleteUser(@RequestParam int id){
        System.out.println(id);
        ResMessage resMessage = new ResMessage();
        Boolean status = userService.deleteUser(id);
        resMessage.setStatus(status);
        if(!status){
            resMessage.setMessage("删除失败");
        }else{
            resMessage.setMessage("删除成功");
        }
        return resMessage;
    }
}
