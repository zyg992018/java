package com.zyg.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zyg.entity.User;
import com.zyg.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired(required = true)
    private UserMapper userMapper;
    public PageInfo<User> getUserList(int page, int limit){
        PageHelper.startPage(page,limit);

        List<User> users = userMapper.getAllUsers();
        PageInfo<User> pageInfo = new PageInfo<User>(users);
        return pageInfo;
    }

    public Boolean deleteUser(int id){
        int row = userMapper.deleteUser(id);
        if(row!=0){
            return true;
        }else{
            return false;
        }
    }

}
