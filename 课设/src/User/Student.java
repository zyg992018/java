package User;

public class Student extends UserImpl {
    private AssistantTeacher assistantTeacher;
    public Student(String username, String password, String phone) {
        super(username, password, phone);
        this.assistantTeacher = null;
    }

    public void setAssistantTeacher(AssistantTeacher assistantTeacher) {
        this.assistantTeacher = assistantTeacher;
    }
}
