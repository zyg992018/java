package User;

public interface User {
    public Integer login();
    public Integer register(String _username, String _password);
    public Integer update();
    public Integer set();
}
