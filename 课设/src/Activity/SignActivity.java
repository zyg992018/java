package Activity;

import java.sql.Date;

public abstract class SignActivity implements Activity{
    private Date startDate;
    private Date endDate;

    public abstract void setWay();

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public void startActivity() {

    }

    @Override
    public void endActivity() {

    }

    @Override
    public void setTime(Date date) {

    }

    @Override
    public void addContent() {

    }
}
