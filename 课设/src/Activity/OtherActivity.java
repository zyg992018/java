package Activity;

import java.sql.Date;

public abstract class OtherActivity implements  Activity{
    private Date startDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private Date endDate;
    @Override
    public void startActivity() {

    }

    @Override
    public void endActivity() {

    }

    @Override
    public void setTime(Date date) {

    }

    @Override
    public void addContent() {

    }
}
