package Activity;

interface ActFactory {
    public Activity createActivity(String activityType);
}
