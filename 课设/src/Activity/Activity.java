package Activity;

import java.sql.Date;

public interface Activity {

    public void startActivity();
    public void endActivity();
    public void setTime(Date date);
    public void addContent();
}
