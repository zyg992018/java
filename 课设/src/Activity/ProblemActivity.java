package Activity;

import java.sql.Date;
import java.util.List;

public abstract class ProblemActivity implements Activity{

    private Date startDate;
    private Date endDate;
    List<Problem> problems;

    public abstract void setWay();

    public void AddProblem(Problem problem){

    }

    public void deleteProblem(Problem problem){

    }
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public void startActivity() {

    }

    @Override
    public void endActivity() {

    }

    @Override
    public void setTime(Date date) {

    }

    @Override
    public void addContent() {

    }
}
