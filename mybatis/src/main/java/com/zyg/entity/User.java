package com.zyg.entity;

import java.sql.Date;

public class User {
    private int id;
    private String username;
    private String password;
    private Byte gender;
    private Date regist_time;

    public User() {
    }

    @Override
    public String toString() {
        return "UserDao{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", gender=" + gender +
                ", regist_time=" + regist_time +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Date getRegist_time() {
        return regist_time;
    }

    public void setRegist_time(Date regist_time) {
        this.regist_time = regist_time;
    }

    public User(int id, String username, String password, Byte gender, Date regist_time) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.regist_time = regist_time;
    }
}
