package com.zyg.dao;

import com.zyg.entity.User;

public interface UserDao{
    public User queryId(int id);
    public void insert(String username,String password,Byte gender);
}